<?php
/**
  * Setup ACF
  */
 class ElementaryWordpressContactDetailsAcf
 {

 	function __construct()
 	{
        $this->register_acf_options();
        $this->add_options_pages();
        add_action('acf/init', [$this, 'setup_filters'] );
    }

    function setup_filters()
    {
        add_filter('acf/load_field/name=social_network', [$this, 'acf_load_color_field_choices'] );
        add_filter('sage/template/app-data/data', [$this, 'add_contact_details_to_templates'] ); // All pages
    }

    // Populate the Social Network selects with the available icons of the icon font
    function acf_load_color_field_choices( $field )
    {

        // Reset choices
        $field['social_networks'] = [];

        // Big list of social networks for the ACF dropdown
        $social_networks = [
            '500px', '8tracks', 'Airbnb', 'Alliance', 'Amazon', 'amplement', 'Android', 'AngelList', 'Apple', 'appnet', 'baidu', 'bandcamp', 'BATTLE.NET', 'Mixer', 'beBee', 'bebo', 'Behance', 'Blizzard', 'Blogger', 'Buffer', 'Chrome', 'coderwall', 'Curse', 'Dailymotion', 'DEEZER', 'Delicious', 'deviantART', 'Diablo', 'Digg', 'Discord', 'DISQUS', 'douban', 'draugiem.lv', 'dribbble', 'Drupal', 'ebay', 'Ello', 'endomondo', 'Envato', 'Etsy', 'Facebook', 'FeedBurner', 'FILMWEB', 'Firefox', 'Flattr', 'flickr', 'FORMULR', 'Forrst', 'foursquare', 'FriendFeed', 'GitHub', 'goodreads', 'Google', 'Google Groups', 'Google Photos', 'Google+', 'Google Scholar', 'grooveshark', 'HackerRank', 'Hearthstone', 'Hellocoton', 'Hereos of the Storm', 'hitbox', 'Horde', 'houzz', 'icq', 'Identica', 'IMDb', 'Instagram', 'issuu', 'iStock', 'iTunes', 'Keybase', 'Lanyrd', 'last.fm', 'LINE', 'Linkedin', 'LiveJournal', 'lyft', 'macOS', 'Mail', 'Medium', 'Meetup', 'Mixcloud', 'Model Mayhem', 'mumble', 'Myspace', 'NewsVine', 'Nintendo Network', 'npm', 'Odnoklassniki', 'OpenID', 'Opera', 'Outlook', 'Overwatch', 'Patreon', 'Paypal', 'Periscope', 'Mozilla Persona', 'Pinterest', 'Google Play', 'Player.me', 'PlayStation', 'Pocket', 'QQ', 'Quora', 'RaidCall', 'Ravelry', 'reddit', 'renren', 'ResearchGate', 'Resident Advisor', 'REVERBNATION', 'RSS', 'ShareThis', 'skype', 'SlideShare', 'SmugMug', 'Snapchat', 'songkick', 'soundcloud', 'Spotify', 'StackExchange', 'stackoverflow', 'Starcraft', 'StayFriends', 'steam', 'Storehouse', 'STRAVA', 'StreamJar', 'StumbleUpon', 'Swarm', 'TeamSpeak', 'TeamViewer', 'Technorati', 'Telegram', 'tripadvisor', 'Tripit', 'triplej', 'tumblr', 'Twitch', 'Twitter', 'UBER', 'Ventrilo', 'Viadeo', 'Viber', 'viewbug', 'vimeo', 'Vine', 'VKontakte', 'Warcraft', 'WeChat', 'Sina Weibo', 'WhatsApp', 'Wikipedia', 'Windows', 'WordPress', 'wykop', 'XBOX', 'Xing', 'Yahoo!', 'Yammer', 'Yandex', 'yelp', 'Younow', 'YouTube', 'Zapier', 'Zerply', 'zomato', 'zynga', 'spreadshirt', 'Trello', 'Game Jolt', 'tunein', 'BLOGLOVIN', 'GameWisp', 'Facebook Messenger', 'Pandora', 'Microsoft', 'mobcrush', 'Sketchfab', 'Youtube Gaming', 'FYUSE', 'Bitbucket', 'AUGMENT', 'ToneDen', 'niconico', 'Zillow', 'Google Maps', 'Booking', 'Fundable', 'Upwork', 'ghost', 'loomly', 'Trulia', 'Ask', 'Gust', 'Toptal', 'Squarespace', 'bonanza', 'Doodle', 'Bing', 'Seedrs', 'Freelancer', 'Shopify', 'Google Calendar', 'Redfin', 'Wix', 'Craigslist', 'Alibaba', 'Zoom', 'Homes', 'Appstore', 'Guru', 'Aliexpress', 'GoToMeeting', 'Fiverr', 'LogMeIn', 'OpenAI Gym', 'Slack', 'Codepen', 'Angie\'s List', 'HomeAdvisor', 'Unsplash', 'Mastodon', 'Natgeo', 'Qobuz', 'Tidal', 'Realtor', 'Calendly', 'homify', 'crunchbase', 'livemaster', 'udemy', 'Nextdoor', 'Origin', 'CodeRED', 'Adobe Portfolio', 'instructables', 'GitLab', 'Mail.ru', 'BookBub', 'Kobo', 'Smashwords', 'Hacker News', 'hackerone', 'beatport', 'napster', 'SPIP', 'Wickr', 'BlackBerry', 'MyAnimeList', 'pixiv', 'GameFor', 'Traxsource', 'indie DB', 'Mod DB' ];

        // remove any unwanted white space
        $social_networks = array_map('trim', $social_networks);

        // loop through array and add to field 'social_networks'
        if( is_array($social_networks) ) {

            foreach( $social_networks as $social_network ) {

                $social_network_clean = $this->clean_text($social_network);

                $field['choices'][ $social_network_clean ] = $social_network;

            }
        }

        return $field;

    }

    function register_acf_options()
    {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_5c66e8276848c',
                'title' => 'Contact Details',
                'fields' => array(
                    array(
                        'key' => 'field_5c66e82d70978',
                        'label' => 'Telephone Number',
                        'name' => 'telephone_number',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5c66e83670979',
                        'label' => 'Email Address',
                        'name' => 'email_address',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5c66e89f7097d',
                        'label' => 'Enable Social Networks',
                        'name' => 'enable_social_networks',
                        'type' => 'checkbox',
                        'instructions' => 'Enables usage of this plugins social icons. An example template is included in the plugin directory. Use the shortcode [\'ed-social\'] to output all networks in an unordered list.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'enable' => 'Enable',
                        ),
                        'allow_custom' => 0,
                        'default_value' => array(
                        ),
                        'layout' => 'vertical',
                        'toggle' => 0,
                        'return_format' => 'value',
                        'save_custom' => 0,
                    ),
                    array(
                        'key' => 'field_5c66e86b7097c',
                        'label' => 'Load icon font',
                        'name' => 'load_icon_font',
                        'type' => 'checkbox',
                        'instructions' => 'Loads in an icon font to be used for the icons.

            Use in this format:
            <span class="ed-facebook"></span>
            <span class="ed-twitter"></span>',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5c66e89f7097d',
                                    'operator' => '==',
                                    'value' => 'enable',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'enable' => 'Enable',
                        ),
                        'allow_custom' => 0,
                        'default_value' => array(
                        ),
                        'layout' => 'vertical',
                        'toggle' => 0,
                        'return_format' => 'value',
                        'save_custom' => 0,
                    ),
                    array(
                        'key' => 'field_5c66e8457097a',
                        'label' => 'Social Links',
                        'name' => 'social_links',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5c66e89f7097d',
                                    'operator' => '==',
                                    'value' => 'enable',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'row',
                        'button_label' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5c66e85f7097b',
                                'label' => 'Social Network',
                                'name' => 'social_network',
                                'type' => 'select',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'choices' => array(
                                ),
                                'default_value' => array(
                                ),
                                'allow_null' => 0,
                                'multiple' => 0,
                                'ui' => 0,
                                'return_format' => 'value',
                                'ajax' => 0,
                                'placeholder' => '',
                                'social_networks' => array(
                                ),
                            ),
                            array(
                                'key' => 'field_5c66e8f47097e',
                                'label' => 'Link',
                                'name' => 'link',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '#',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'contact-details',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            endif;
    }

    // Add ACF options pages
    function add_options_pages()
    {
        if (function_exists('acf_add_options_page'))
        {
            acf_add_options_page(array(
                'page_title'    => 'Contact Details',
                'menu_title'    => 'Contact Details',
                'menu_slug'     => 'contact-details',
                'icon_url'      => 'dashicons-email-alt',
                'capability'    => 'edit_posts',
                'redirect'      => false
            ));
        }
    }

    // Add all the contact options to ALL templates in Sage 9
    // Usage: {{ $contact_details['telephone-number'] }}
    function add_contact_details_to_templates(array $data) {

        $options = get_fields('options');

        if ($options) {
            foreach ($options as $key => $content) {
                $data['contact_details'][$key] = $content;
            }
        }

        // sage('blade')->share('contact_details', $data);
        return $data;
    }

    // Cleanup text to hyphens and lowercase
	function clean_text($str, $replace=array(), $delimiter='-')
    {
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = preg_replace(array('/Ä/', '/Ö/', '/Ü/', '/ä/', '/ö/', '/ü/'), array('Ae', 'Oe', 'Ue', 'ae', 'oe', 'ue'), $str);
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $clean);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }


 }