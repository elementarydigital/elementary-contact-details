=== Elementary Contact Details Plugin ===
Contributors: jamesfawcett
Donate link: https://www.elementarydigital.co.uk
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This module is for creating a contact details sidebar tab, including social icons.

Installation:

1. Add the repository to your composer.json file:

```
"repositories": [

    {
      "type": "vcs",
      "url": "git@bitbucket.org:elementarydigital/elementary-contact-details.git"
    },

  ],
```

2. Add the dependency:
`"elementarydigital/elementary-contact-details": "*",`

3. Run `composer update` or `composer install`

4. Usage example for the social icons:
```
@if (!empty($contact_details['enable_social_networks']))
	@foreach ( $contact_details['social_links'] as $social )
	  <a href="{{ $social['link'] }}"><span class="edicon edicon-{{ $social['social_network'] }}"></span></a>
	@endforeach
@endif
```

== Changelog ==

= 1.0.0 =
* Initial Release

= 1.0.2 =
* Remove unused template files, correction to README.

= 1.0.3 =
* Add optional social icon font

= 1.0.4 =
* Loading header/footer scripts taken carre of by site options plugin instead

