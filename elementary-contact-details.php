<?php
/**
 * Plugin Name:       Elementary Contact Details
 * Plugin URI:        https://www.elementarydigital.co.uk
 * Description:       Elementary Contact Details Wordpress plugin
 * Version:           1.0.3
 * Author:            James Fawcett
 * Author URI:        https://www.elementarydigital.co.uk
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ElementaryWordpressContactDetails {

    public function __construct() {

    	define( 'ELEMENTARY_CONTACT_DETAILS_CORE_PATH', plugin_dir_path( __FILE__ ) );
		define( 'ELEMENTARY_CONTACT_DETAILS_CORE_URL', plugin_dir_url( __FILE__ ) );

    	$this->settings = array(
			'name'    => 'Elementary Wordpress Contact Details',
			'version' => '1.0.3',
		);

        // add_action('wp_enqueue_scripts', [$this, 'enqueue']);
        add_action('wp_footer', [$this, 'enqueue']); // Moved to footer - @TODO check this is final

		$this->initialise();
    }

    public function initialise()
	{
        // Load ACF requirements
        require_once('includes/class-elementary-contact-details-acf.php');
        $ElementaryWordpressContactDetailsAcf = new ElementaryWordpressContactDetailsAcf;
	}

    /**
     * Add the icon fonts (if required)
     */
    public function enqueue() {

        // First, check the icon font is enabled in the admin
        if (!get_field( 'load_icon_font', 'options' )) return;

        wp_enqueue_style(
            'elementary-contact-details-styles',
            ELEMENTARY_CONTACT_DETAILS_CORE_URL . 'assets/styles/style.css'
        );
    }
}

$ElementaryWordpressContactDetails = new ElementaryWordpressContactDetails();
