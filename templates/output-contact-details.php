<?php
/**
 * The template file for the Wordpress Downloads Plugin
 * This can be overwritten by copying it to your theme folder
 * YOURTHEME/views/components/wordpress-downloads-template.php
 */
?>

<?php do_action('wordpress_downloads_before'); ?>

<div class="wordpress-downloads <?php do_action('wordpress_downloads_classes') ?>">

    <?php do_action('wordpress_downloads_above_downloads'); ?>

    <div class="wordpress-downloads__wrapper">

        <?php
            // @TODO
        ?>

        <?php do_action('wordpress_downloads_below_downloads'); ?>

    </div>
</div>

<?php do_action('wordpress_downloads_after'); ?>